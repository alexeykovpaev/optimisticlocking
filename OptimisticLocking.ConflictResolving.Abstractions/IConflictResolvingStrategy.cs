using System;
using System.Threading;
using System.Threading.Tasks;

namespace OptimisticLocking.ConflictResolving.Abstractions
{
    public interface IConflictResolvingStrategy<T> where T : ICloneable, IEquatable<T>
    {
        Task<Versioned<T>> HandleWriteAsync(
            Versioned<T> modifiedValue,
            FetchDelegate<T> fetch,
            SaveDelegate<T> save,
            CancellationToken cancellationToken);
    }
}