using System;
using System.Threading;
using System.Threading.Tasks;

namespace OptimisticLocking.ConflictResolving.Abstractions
{
    public delegate Task SaveDelegate<T>(Versioned<T> value, CancellationToken cancellationToken) where T : ICloneable, IEquatable<T>;
}