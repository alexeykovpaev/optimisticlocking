using System;
using System.Threading;
using System.Threading.Tasks;

namespace OptimisticLocking.ConflictResolving.Abstractions
{
    public delegate Task<Versioned<T>> FetchDelegate<T>(CancellationToken cancellationToken) where T : ICloneable, IEquatable<T>;
}