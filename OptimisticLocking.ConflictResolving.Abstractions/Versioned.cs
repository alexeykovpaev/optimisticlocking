using System;

namespace OptimisticLocking.ConflictResolving.Abstractions
{
    public struct Versioned<T> where T : ICloneable, IEquatable<T>
    {
        public T CurrentValue { get; }
        public T InitialValue { get; }
        public int InitialVersion { get; }

        public bool HasChanged => !InitialValue.Equals(CurrentValue);
        
        public Versioned(T value, int version)
        {
            CurrentValue = value;
            InitialValue = (T) value.Clone();
            InitialVersion = version;
        }
    }
}