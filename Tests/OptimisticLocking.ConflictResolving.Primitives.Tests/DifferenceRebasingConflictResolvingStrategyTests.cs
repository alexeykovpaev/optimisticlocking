using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using OptimisticLocking.ConflictResolving.Abstractions;
using OptimisticLocking.DataAccess.Abstractions.Exceptions;
using Xunit;

namespace OptimisticLocking.ConflictResolving.Primitives.Tests
{
    public class DifferenceRebasingConflictResolvingStrategyTests
    {
        
        // |        Action        |      DB       | Result |
        // |----------------------|---------------|--------|
        // | Initial read         | 3, version: 1 |        |
        // | Write(5, version: 1) | 5, version: 2 | OK     |
        [Fact]
        public async Task HandleWriteAsync_WhenNoConflict_ShouldWriteAndUpdateVersion()
        {
            // Arrange
            var strategy = GetStrategy();
            var mockRepository = new Mock<IRepository<Data>>();
            var repository = mockRepository.Object;
            var modifiedValue = new Versioned<Data>(Data.From(3), 1);
            modifiedValue.CurrentValue.Value = 5;
            
            // Act
            var result = await strategy.HandleWriteAsync(
                modifiedValue,
                repository.FetchAsync,
                repository.SaveAsync,
                CancellationToken.None);

            // Assert
            mockRepository.Verify(r => r.FetchAsync(CancellationToken.None), Times.Never);
            mockRepository.Verify(r => r.SaveAsync(
                    modifiedValue,
                    CancellationToken.None),
                Times.Once);
            result.Should().BeEquivalentTo(new Versioned<Data>(Data.From(5), 2));
        }

        
        // |        Action        |      DB       |    Result     |
        // |----------------------|---------------|---------------|
        // | Initial read         | 3, version: 1 |               |
        // | !!! Concurrent write | 5, version: 2 |               |
        // | Write(5, version: 1) |               | Err           |
        // | Read                 |               | 5, version: 2 |
        // | Write(7, version: 2) | 7, version: 3 | OK            |
        [Fact]
        public async Task HandleWriteAsync_WhenVersionConflict_ShouldResolveViaRebasingAndReturnUpdated()
        {
            // Arrange
            var strategy = GetStrategy();
            var mockRepository = new Mock<IRepository<Data>>();
            var repository = mockRepository.Object;
            var modifiedValue = new Versioned<Data>(Data.From(3), 1);
            modifiedValue.CurrentValue.Value = 5;

            mockRepository.SetupSequence(r => r.FetchAsync(CancellationToken.None))
                          .ReturnsAsync(new Versioned<Data>(Data.From(5), 2));
            mockRepository.SetupSequence(r => r.SaveAsync(It.IsAny<Versioned<Data>>(), CancellationToken.None))
                          .Throws(new RecordVersionMismatchException("key", 2, 1))
                          .Returns(Task.CompletedTask);
            
            // Act
            var result = await strategy.HandleWriteAsync(
                modifiedValue,
                repository.FetchAsync,
                repository.SaveAsync,
                CancellationToken.None);
            
            // Assert
            mockRepository.Verify(r => r.FetchAsync(CancellationToken.None), Times.Once);
            mockRepository.Verify(r => r.SaveAsync(
                    It.IsAny<Versioned<Data>>(),
                    CancellationToken.None),
                Times.Exactly(2));
            result.Should().BeEquivalentTo(new Versioned<Data>(Data.From(7), 3));
        }

        private DifferenceRebasingConflictResolvingStrategy<Data, Data> GetStrategy()
        {
            var rebaser = new GenericDifferenceRebaser<Data, Data>(
                value => Data.From(value.CurrentValue.Value - value.InitialValue.Value),
                (target, diff) => target.CurrentValue.Value += diff.Value);
            return new DifferenceRebasingConflictResolvingStrategy<Data, Data>(rebaser);
        }
    }
}