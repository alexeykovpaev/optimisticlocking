using System;
using System.Threading;
using System.Threading.Tasks;
using OptimisticLocking.ConflictResolving.Abstractions;

namespace OptimisticLocking.ConflictResolving.Primitives.Tests
{
    public class Repository<T> : IRepository<T> where T : ICloneable, IEquatable<T>
    {
        private readonly FetchDelegate<T> _fetch;
        private readonly SaveDelegate<T> _save;

        public Repository(FetchDelegate<T> fetch, SaveDelegate<T> save)
        {
            _fetch = fetch;
            _save = save;
        }

        public Task<Versioned<T>> FetchAsync(CancellationToken cancellationToken)
            => _fetch(cancellationToken);

        public Task SaveAsync(Versioned<T> value, CancellationToken cancellationToken)
            => _save(value, cancellationToken);
    }
}