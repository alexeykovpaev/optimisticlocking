using System;

namespace OptimisticLocking.ConflictResolving.Primitives.Tests
{
    public class Data : ICloneable, IEquatable<Data>
    {
        public int Value { get; set; }

        public object Clone()
        {
            return new Data { Value = Value };
        }
        
        public bool Equals(Data other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Data) obj);
        }

        public override int GetHashCode() => Value;

        public override string ToString() => Value.ToString();

        public static Data From(int value) => new Data { Value = value };
    }
}