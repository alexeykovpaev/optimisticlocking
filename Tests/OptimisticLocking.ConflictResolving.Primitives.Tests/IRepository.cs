using System;
using System.Threading;
using System.Threading.Tasks;
using OptimisticLocking.ConflictResolving.Abstractions;

namespace OptimisticLocking.ConflictResolving.Primitives.Tests
{
    public interface IRepository<T> where T : ICloneable, IEquatable<T>
    {
        Task<Versioned<T>> FetchAsync(CancellationToken cancellationToken);
        Task SaveAsync(Versioned<T> value, CancellationToken cancellationToken);
    }
}