﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using OptimisticLocking.DataAccess.Abstractions.Exceptions;
using Xunit;

namespace OptimisticLocking.DataAccess.InMemory
{
    public class InMemoryDatabaseTests
    {
        #region ReadAsync

        [Fact]
        public async Task ReadAsync_WhenNoKeyExists_ShouldReturnNull()
        {
            // Arrange
            var sut = new InMemoryDatabaseBuilder().Build();
            
            // Act
            var result = await sut.ReadAsync<Data>("non_existed_key");
            
            // Assert
            result.data.Should().BeNull();
            result.version.Should().Be(-1);
        }

        #endregion

        #region CreateAsync

        [Fact]
        public async Task CreateAsync_WhenNoKeyExists_ShouldNotThrow()
        {
            // Arrange
            var sut = new InMemoryDatabaseBuilder().Build();
            
            // Act & Arrange
            await sut.CreateAsync("non_existed_key", new Data { Amount = 10 });
        }

        [Fact]
        public void CreateAsync_WhenKeyAlreadyExists_ShouldThrow()
        {
            // Arrange
            var key = "existed_key";
            var initialValue = new Data { Amount = 10 };
            var sut = new InMemoryDatabaseBuilder()
                     .WithNewValue(key, initialValue)
                     .Build();
            
            // Act
            Func<Task> act = () => sut.CreateAsync(key, new Data { Amount = 20 });
            
            // Assert
            act.Should().ThrowAsync<RecordAlreadyExistsException>();
        }
        
        #endregion

        #region UpdateAsync

        [Fact]
        public async Task UpdateAsync_WhenKeyWithSpecifiedVersionExists_ShouldUpdate()
        {
            // Arrange
            var key = "existed_key";
            var initialValue = new Data { Amount = 10 };
            var updateValue = new Data { Amount = 20 };
            var sut = new InMemoryDatabaseBuilder()
                     .WithValue(key, initialValue, version: 10)
                     .Build();
            
            // Act
            await sut.UpdateAsync(key, updateValue, 10);
            var updatedValue = await sut.ReadAsync<Data>(key);

            // Assert
            updatedValue.data.Should().BeEquivalentTo(updateValue);
            updatedValue.version.Should().Be(11);
        }
        
        [Fact]
        public void UpdateAsync_WhenNoKeyExists_ShouldThrow()
        {
            // Arrange
            var key = "non_existed_key";
            var sut = new InMemoryDatabaseBuilder().Build();
            
            // Act
            Func<Task> act = () => sut.UpdateAsync(key, new Data { Amount = 10 }, expectedVersion: 0);
            
            // Assert
            act
               .Should().ThrowAsync<RecordNotFoundException>()
               .Result.Which.Key
               .Should().BeEquivalentTo(key);
        }

        [Fact]
        public void UpdateAsync_WhenKeyHasDifferentVersionThanWasSpecified_ShouldThrow()
        {
            // Arrange
            var key = "existed_key";
            var sut = new InMemoryDatabaseBuilder()
                     .WithValue(key, new Data { Amount = 10 }, 5)
                     .Build();
            
            // Act
            Func<Task> act = () => sut.UpdateAsync(key, new Data { Amount = 20 }, 4);
            
            // Assert
            act
               .Should().ThrowAsync<RecordVersionMismatchException>()
               .Result.Which.ActualVersion
               .Should().Be(5);
        }
        
        #endregion
    }
}