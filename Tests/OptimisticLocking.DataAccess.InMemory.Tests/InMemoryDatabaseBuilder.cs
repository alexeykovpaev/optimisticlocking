using System.Collections.Generic;

namespace OptimisticLocking.DataAccess.InMemory
{
    public class InMemoryDatabaseBuilder
    {
        private readonly Dictionary<string, Record> _initialData = new Dictionary<string, Record>();

        public InMemoryDatabaseBuilder WithNewValue<T>(string key, T value)
        {
            var record = Record.FromNew(value);
            
            _initialData.Add(key, record);
            
            return this;
        }
        
        public InMemoryDatabaseBuilder WithValue<T>(string key, T value, int version)
        {
            var record = Record.From(value, version);
            
            _initialData.Add(key, record);
            
            return this;
        }
        
        public InMemoryDatabase Build()
        {
            return new InMemoryDatabase(_initialData);
        }
    }
}