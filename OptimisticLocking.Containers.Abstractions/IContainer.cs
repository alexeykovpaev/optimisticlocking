using System.Threading;
using System.Threading.Tasks;

namespace OptimisticLocking.Containers.Abstractions
{
    public interface IContainer<T>
    {
        Task<T> GetAsync(CancellationToken cancellationToken);
        Task SaveChanges(CancellationToken cancellationToken);
    }
}