using System.Threading.Tasks;

namespace OptimisticLocking.DataAccess.Abstractions
{
    public interface IDatabase
    {
        Task<(T data, int version)> ReadAsync<T>(string key);
        Task CreateAsync<T>(string key, T data);
        Task UpdateAsync<T>(string key, T data, int expectedVersion);
    }
}