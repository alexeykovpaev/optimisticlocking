using System;

namespace OptimisticLocking.DataAccess.Abstractions.Exceptions
{
    public class DataAccessException : Exception
    {
        public DataAccessException(string message, Exception innerException = null)
            : base(message, innerException = null)
        {
        }
    }
}