namespace OptimisticLocking.DataAccess.Abstractions.Exceptions
{
    public class RecordAlreadyExistsException : DataAccessException
    {
        public string Key { get; }

        public RecordAlreadyExistsException(string key)
            : base($"Record already exists. Key: {key}")
        {
            Key = key;
        }
    }
}