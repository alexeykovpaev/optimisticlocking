namespace OptimisticLocking.DataAccess.Abstractions.Exceptions
{
    public class RecordNotFoundException : DataAccessException
    {
        public string Key { get; }
        
        public RecordNotFoundException(string key)
            : base($"Record does not exist. Key: {key}")
        {
        }
    }
}