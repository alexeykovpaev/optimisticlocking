namespace OptimisticLocking.DataAccess.Abstractions.Exceptions
{
    public class RecordVersionMismatchException : DataAccessException
    {
        public string Key { get; }
        public int ActualVersion { get; }
        public int ProvidedVersion { get; }

        public RecordVersionMismatchException(string key, int actualVersion, int providedVersion)
            : base($"Record has conflicted version. Key: {key}, Actual version: {actualVersion}, Provided version: {providedVersion}")
        {
            Key = key;
            ActualVersion = actualVersion;
            ProvidedVersion = providedVersion;
        }
    }
}