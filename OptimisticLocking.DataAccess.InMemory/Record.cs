namespace OptimisticLocking.DataAccess.InMemory 
{
    public struct Record
    {
        internal const int NotExistedVersion = -1;
        internal const int BrandNewVersion = 0;
        
        public string Data { get; }
        public int Version { get; }

        public Record(string data, int version)
        {
            Data = data;
            Version = version;
        }

        public T As<T>() => SerializationHelper.Deserialize<T>(Data);
        
        public static Record FromNew<T>(T value)
        {
            return From(value, BrandNewVersion);
        }

        public static Record From<T>(T value, int version)
        {
            return new Record(SerializationHelper.Serialize(value), version);
        }
    }
}