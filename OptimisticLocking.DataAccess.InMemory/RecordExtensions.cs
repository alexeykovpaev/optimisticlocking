using Newtonsoft.Json;

namespace OptimisticLocking.DataAccess.InMemory
{
    public static class SerializationHelper
    {
        public static string Serialize<T>(T value) => JsonConvert.SerializeObject(value);
        
        public static T Deserialize<T>(string data) => JsonConvert.DeserializeObject<T>(data);
    }
}