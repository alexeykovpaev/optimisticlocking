using System.Threading;
using System.Threading.Tasks;

namespace OptimisticLocking.DataAccess.InMemory
{
    internal class RecordGuard
    {
        private readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1);
        
        public Record Record { get; set; }

        public RecordGuard(Record record)
        {
            Record = record;
        }
        
        public Task WaitAsync(CancellationToken cancellationToken = default) => _semaphoreSlim.WaitAsync(cancellationToken);

        public void Release() => _semaphoreSlim.Release();
    }
}