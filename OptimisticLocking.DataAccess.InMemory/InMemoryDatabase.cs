﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using OptimisticLocking.DataAccess.Abstractions;
using OptimisticLocking.DataAccess.Abstractions.Exceptions;

namespace OptimisticLocking.DataAccess.InMemory
{
    [SuppressMessage("ReSharper", "InconsistentlySynchronizedField")]
    public class InMemoryDatabase : IDatabase
    {
        private readonly ConcurrentDictionary<string, RecordGuard> _storage;

        public InMemoryDatabase(IDictionary<string, Record> initialState)
        {
            var guardedInitialState = initialState.ToDictionary(
                x => x.Key, 
                x => new RecordGuard(x.Value));
            
            _storage = new ConcurrentDictionary<string, RecordGuard>(guardedInitialState);
        }
        
        public Task<(T data, int version)> ReadAsync<T>(string key)
        {
            if (_storage.TryGetValue(key, out var guard))
            {
                var record = guard.Record;
                return Task.FromResult((SerializationHelper.Deserialize<T>(record.Data), record.Version));
            }

            return Task.FromResult((default(T), Record.NotExistedVersion));
        }

        public Task CreateAsync<T>(string key, T data)
        {
            lock (_storage)
            {
                if (_storage.TryGetValue(key, out var guard))
                {
                    var record = guard.Record;
                    throw new RecordAlreadyExistsException(key);
                }

                _storage.GetOrAdd(key, new RecordGuard(Record.FromNew(data)));
            }
            
            return Task.CompletedTask;
        }
        
        public async Task UpdateAsync<T>(string key, T data, int expectedVersion)
        {
            if (_storage.TryGetValue(key, out var guard))
            {
                try
                {
                    await guard.WaitAsync();
                    
                    var record = guard.Record;
                    if (record.Version == expectedVersion)
                    {
                        guard.Record = Record.From(data, expectedVersion + 1);
                    }
                    else
                    {
                        throw new RecordVersionMismatchException(key, record.Version, expectedVersion);
                    }
                }
                finally
                {
                    guard.Release();
                }
            }
            else
            {
                throw new RecordNotFoundException(key);
            }
        }
    }
}