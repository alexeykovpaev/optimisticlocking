using System;
using OptimisticLocking.ConflictResolving.Abstractions;

namespace OptimisticLocking.ConflictResolving.Primitives
{
    public class GenericDifferenceRebaser<T, TDiff> : IDifferenceRebaser<T, TDiff> where T : ICloneable, IEquatable<T>
    {
        private readonly Func<Versioned<T>, TDiff> _getDifference;
        private readonly Action<Versioned<T>, TDiff> _applyDifference;

        public GenericDifferenceRebaser(
            Func<Versioned<T>, TDiff> getDifference,
            Action<Versioned<T>, TDiff> applyDifference)
        {
            _getDifference = getDifference;
            _applyDifference = applyDifference;
        }
        
        public TDiff GetDifference(Versioned<T> value) => _getDifference(value);

        public void ApplyDifference(Versioned<T> target, TDiff difference) => _applyDifference(target, difference);
    }
}