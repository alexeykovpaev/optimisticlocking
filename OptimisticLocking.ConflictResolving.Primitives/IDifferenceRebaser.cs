using System;
using OptimisticLocking.ConflictResolving.Abstractions;

namespace OptimisticLocking.ConflictResolving.Primitives
{
    public interface IDifferenceRebaser<T, TDiff> where T : ICloneable, IEquatable<T>
    {
        TDiff GetDifference(Versioned<T> value);
        void ApplyDifference(Versioned<T> target, TDiff difference);
    }
}