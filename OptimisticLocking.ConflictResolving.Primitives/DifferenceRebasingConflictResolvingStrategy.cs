using System;
using System.Threading;
using System.Threading.Tasks;
using OptimisticLocking.ConflictResolving.Abstractions;
using OptimisticLocking.DataAccess.Abstractions.Exceptions;

namespace OptimisticLocking.ConflictResolving.Primitives
{
    public class DifferenceRebasingConflictResolvingStrategy<T, TDiff> : IConflictResolvingStrategy<T> where T : ICloneable, IEquatable<T>
    {
        private readonly IDifferenceRebaser<T, TDiff> _differenceRebaser;

        public DifferenceRebasingConflictResolvingStrategy(IDifferenceRebaser<T, TDiff> differenceRebaser)
        {
            _differenceRebaser = differenceRebaser;
        }
        
        public async Task<Versioned<T>> HandleWriteAsync(
            Versioned<T> modifiedValue,
            FetchDelegate<T> fetch,
            SaveDelegate<T> save,
            CancellationToken cancellationToken)
        {
            try
            {
                await save(modifiedValue, cancellationToken);
                return new Versioned<T>(modifiedValue.CurrentValue, modifiedValue.InitialVersion + 1);
            }
            catch (RecordVersionMismatchException _)
            {
                var targetValue = await fetch(cancellationToken);
                var diff = _differenceRebaser.GetDifference(modifiedValue);
                _differenceRebaser.ApplyDifference(targetValue, diff);

                return await HandleWriteAsync(targetValue, fetch, save, cancellationToken);
            }
        }
    }
}