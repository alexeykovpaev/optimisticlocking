using System;
using System.Threading;
using System.Threading.Tasks;
using OptimisticLocking.ConflictResolving.Abstractions;
using OptimisticLocking.Containers.Abstractions;

namespace OptimisticLocking.Containers.ConflictsFree
{
    public class Container<T> : IContainer<T> where T : ICloneable, IEquatable<T>
    {
        private readonly FetchDelegate<T> _fetch;
        private readonly SaveDelegate<T> _save;
        private readonly IConflictResolvingStrategy<T> _conflictResolvingStrategy;

        private Lazy<FetchDelegate<T>> _containedValue;
        
        public Container(
            FetchDelegate<T> fetch,
            SaveDelegate<T> save,
            IConflictResolvingStrategy<T> conflictResolvingStrategy)
        {
            _fetch = fetch;
            _save = save;
            _conflictResolvingStrategy = conflictResolvingStrategy;
            
            _containedValue = new Lazy<FetchDelegate<T>>(_fetch);
        }
        
        public async Task<T> GetAsync(CancellationToken cancellationToken)
        {
            return (await _containedValue.Value(cancellationToken)).CurrentValue;
        }

        public async Task SaveChanges(CancellationToken cancellationToken)
        {
            var value = await _containedValue.Value(cancellationToken);
            if (value.HasChanged)
            {
                var freshValue = await _conflictResolvingStrategy.HandleWriteAsync(
                    value,
                    _fetch,
                    _save,
                    cancellationToken);
                
                _containedValue = new Lazy<FetchDelegate<T>>(_ => Task.FromResult(freshValue));
            }
        }
    }
}